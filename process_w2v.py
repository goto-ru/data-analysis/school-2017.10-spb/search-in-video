import pickle
import numpy as np
import pickle
import re
import gensim


def sent2words(sent):
    return filter(lambda x: x not in ['', ' ', '<'], re.sub(r'[^a-z ]+', '', str.lower(sent)).split(' '))

def sents_save(sents, dir_name, w2v):
    MAX_BUCKET = 2000

    bucket, names = [], []
    buc_n = 0
    for id, text in sents:
        bucket.append(text)
        names.append(id)

        if len(bucket) > MAX_BUCKET:
            matr = []
            for sent in bucket:
                words = sent2words(sent)
                sent_vec = []
                for word in words:
                    try:
                        sent_vec.append(w2v[word])
                    except:
                        pass
                matr.append(sent_vec)

            data = list(zip(names, matr))

            with open('{}/w2v/{}_{}.wtov'.format(dir_name, get_cur_time(), buc_n), 'wb') as fl:
                pickle.dump(data, fl)
            bucket, names = [], []
            buc_n += 1
            print('Processed {}'.format(buc_n * MAX_BUCKET))


if __name__ == '__main__':
    with open('data.w2v', 'rb') as fl:
        data = pickle.load(fl)
    print('data loaded')
    dataDir = '.'
    w2v = gensim.models.KeyedVectors.load_word2vec_format(
        'net/GoogleNews-vectors-negative300.bin', binary=True, limit=5 * 10 ** 5)
    print('w2v loaded')
    sents_save(data, dataDir, w2v)
