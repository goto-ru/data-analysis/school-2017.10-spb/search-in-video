from main_net import main
from model_net_3 import build_cnn

dataDir = 'param'
version = 'cnn2_5'

if __name__ == '__main__':
    main(sys.argv, data_dir, build_cnn, version)
