from main_net import main
from model_net_1 import build_cnn

data_dir = 'param'
version = 'cnn2_4'

if __name__ == '__main__':
    main(sys.argv, data_dir, build_cnn, version)
