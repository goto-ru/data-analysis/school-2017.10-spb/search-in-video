import numpy as np
import theano.tensor as T
import theano
import lasagne
from lasagne.layers import *
import lasagne.nonlinearities as nonlin
from net.train import save_net, load_net, NeuralNet

H = 300
W = 14


def build_cnn(file_name=None):
    net = NeuralNet(must_have=[
        'input_shape',
        'learning_rate',
        'train_fun',
        'loss_fun',
        'loss_fun_det',
        'predict_fun_det'])

    input_shape = [None, H, W]

    input_X = T.tensor3("input X", dtype='floatX')
    target_y = T.matrix("target Y", dtype='floatX')

    net['inp'] = InputLayer(input_shape, input_var=input_X)

    net['max'] = GlobalPoolLayer(net['inp'], pool_function=T.max)
    net['min'] = GlobalPoolLayer(net['inp'], pool_function=T.min)
    net['mean'] = GlobalPoolLayer(net['inp'], pool_function=T.mean)

    net['con_2'] = Conv1DLayer(net['inp'], num_filters=64, filter_size=2, nonlinearity=None)
    net['con_3'] = Conv1DLayer(net['inp'], num_filters=64, filter_size=3, nonlinearity=None)
    net['con_4'] = Conv1DLayer(net['inp'], num_filters=64, filter_size=4, nonlinearity=None)

    boltzmann_max = lambda a, axis: T.sum(a * T.exp(a), axis=-1) / T.exp(a).sum(-1)

    net['gmax_2b'] = GlobalPoolLayer(net['con_2'], pool_function=boltzmann_max)
    net['gmax_3b'] = GlobalPoolLayer(net['con_3'], pool_function=boltzmann_max)
    net['gmax_4b'] = GlobalPoolLayer(net['con_4'], pool_function=boltzmann_max)

    net['merge'] = ConcatLayer((net['max'], net['min'], net['mean'],
                                               net['gmax_2b'], net['gmax_3b'], net['gmax_4b']))

    net['batch_0'] = batch_norm(net['merge'])

    net['dens_1'] = DenseLayer(net['batch_0'], num_units=500, nonlinearity=nonlin.elu)
    net['batch_1'] = batch_norm(net['dens_1'])
    net['drop_1'] = DropoutLayer(net['batch_1'], p=0.5)

    net['dens_2'] = DenseLayer(net['drop_1'], num_units=500, nonlinearity=nonlin.elu)
    net['batch_2'] = batch_norm(net['dens_2'])
    net['drop_2'] = DropoutLayer(net['batch_2'], p=0.5)

    net['last'] = DenseLayer(net['drop_2'], num_units=4096)

    y_predicted = get_output(net['last'])
    y_predicted_det = get_output(net['last'], deterministic=True)

    all_weights = get_all_params(net['last'], trainable=True)

    learning_rate = theano.shared(lasagne.utils.floatX(0.001))
    loss = lasagne.objectives.squared_error(target_y, y_predicted).mean()
    loss_det = lasagne.objectives.squared_error(target_y, y_predicted_det).mean()

    updates = lasagne.updates.adam(loss, all_weights, learning_rate=learning_rate)

    train_fun = theano.function([input_X, target_y], loss, updates=updates, allow_input_downcast=True)
    loss_fun = theano.function([input_X, target_y], loss, allow_input_downcast=True)
    loss_fun_det = theano.function([input_X, target_y], loss_det, allow_input_downcast=True)
    predict_fun_det = theano.function([input_X], y_predicted_det, allow_input_downcast=True)

    if file_name:
        load_net(net['last'], file_name)

    net.input_shape = input_shape
    net.learning_rate = learning_rate
    net.train_fun = train_fun
    net.loss_fun = loss_fun
    net.loss_fun_det = loss_fun_det
    net.predict_fun_det = predict_fun_det

    return net.check()
