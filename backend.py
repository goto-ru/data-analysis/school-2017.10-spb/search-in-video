from net.word2vec import *
from net.sents_process import *
from net.helper import procces_x
import zoo_new.model_net_2 as model_net
from net.sents_process import sent2words
from pandas import ewma
from scipy import spatial
import imageio


def frames_load(dir_name):
    frames = {}
    for file_name in os.listdir(dir_name):
        if '.' in file_name and file_name.split('.')[1] == 'data':
            with open('{}/{}'.format(dir_name, file_name), 'rb') as fl:
                data = pickle.load(fl)
                frames.update(data)
    return frames


class TextHandler:
    FRAME_PER_SEC = 25

    def __init__(self, data_dir):
        frames = frames_load(data_dir)
        self.frames = {k: v['fc7'] for k, v in frames.items()}
        with open('test.txt', 'w') as fl:
            for a, b in self.frames.items():
                fl.write(str(a))
                fl.write('\n')
                fl.write(str(b))
        self.w2v = load_w2v(10 ** 5)
        self.net = model_net.build_cnn('zoo_new/epoch_cnn2_3_04-11-2017_22-33-53.net')
        self.video = imageio.get_reader('Africa.mp4')

    def text2vec(self, text):
        words = sent2words(text)
        vecs = []
        bad_words = []
        for word in words:
            try:
                vecs.append(self.w2v[word])
            except:
                bad_words.append(word)
        print("Unknown word:", *bad_words)
        if len(vecs) == 0:
            return None
        return procces_x(vecs, model_net.W)

    def get_obj_by_vec(self, vec):
        def sim(v1, v2):
            return spatial.distance.cosine(v1, v2)

        #def sim(v1, v2):
        #    return np.linalg.norm(v1 - v2)

        return [(id, sim(vec, point)) for id, point in self.frames.items()]

    @staticmethod
    def link_from_time(time, id='8kBGjNI2bwA'):
        return "https://youtu.be/{}?t={}".format(id, time)

    def get_link(self, text):
        vec = self.text2vec(text)
        if vec is None:
            raise Exeption('Bad text')
        vec = self.net.predict_fun_det([vec])[0]
        frames = self.get_obj_by_vec(vec)

        x, y = np.transpose(frames)

        span = 2
        y = (ewma(y, span=span) + ewma(y[::-1], span=span)[::-1]) / 2
        y -= np.min(y)
        y /= np.max(y)

        frames = list(zip(x, y))
        frames.sort(key=lambda x: x[1])

        def pr(t):
            return int(t[0] / TextHandler.FRAME_PER_SEC)
        frames = list(filter(lambda x: pr(x) not in [0, 272, 214], frames))

        for fr, di in frames[:4]:
            print(fr, di)
        print(np.argmin(y))

        frame_id = int(frames[0][0])
        return {
            'link': TextHandler.link_from_time(int(frame_id / TextHandler.FRAME_PER_SEC)),
            'img': self.video.get_data(frame_id),
            'id': frame_id}

    def plot(self, text, span=10):
        import matplotlib.pyplot as plt
        import datetime

        vec = self.text2vec(text)
        vec = self.net.predict_fun_det([vec])[0]
        frames = self.get_obj_by_vec(vec)
        frames.sort(key=lambda x: x[0])

        x, y = np.transpose(frames)

        x = [int(s / TextHandler.FRAME_PER_SEC) for s in x]
        x = np.array([datetime.datetime(2013, 9, 28, 0, s // 60, s % 60) for s in x])

        y = (ewma(y, span=span) + ewma(y[::-1], span=span)[::-1]) / 2
        y -= np.min(y)
        y /= np.max(y)

        y = 1 - y
        y = np.exp(y) / sum(np.exp(y))

        plt.figure(figsize=(10, 4))
        plt.plot(x, y)
        plt.show()
