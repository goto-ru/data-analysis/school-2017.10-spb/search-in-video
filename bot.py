import requests
import datetime
from backend import TextHandler
import imageio

token = ''


class BotHandler:
    def __init__(self, token):
        self.token = token
        self.api_url = "https://api.telegram.org/bot{}/".format(token)

    def get_updates(self, offset=None, timeout=30):
        method = 'getUpdates'
        params = {'timeout': timeout, 'offset': offset}
        resp = requests.get(self.api_url + method, params)
        result_json = resp.json()['result']
        return result_json

    def send_message(self, chat_id, text):
        params = {'chat_id': chat_id, 'text': text}
        method = 'sendMessage'
        resp = requests.post(self.api_url + method, params)
        return resp

    def get_last_update(self):
        get_result = self.get_updates()

        if len(get_result) > 0:
            last_update = get_result[-1]
        else:
            last_update = None

        return last_update

    def send_photo(self, chat_id, img_name):
        method = 'sendPhoto'
        params = {'chat_id': chat_id}
        files = {'photo': open(img_name, 'rb')}
        resp = requests.post(self.api_url + method, params, files=files)
        return resp


def main():
    STAR_TOKEN = ['/s', '/search']
    MAX_PER_MINUTE = 50

    greet_bot = BotHandler(token)
    print('BotHandler created')
    now = datetime.datetime.now()
    text_handler = TextHandler('video_data')
    print('TextHandler created')
    new_offset = None
    minute = now.minute
    delay = {}

    while True:
        greet_bot.get_updates(new_offset)

        last_update = greet_bot.get_last_update()

        if last_update:
            last_update_id = last_update['update_id']
            text = last_update['message']['text']
            chat_id = last_update['message']['chat']['id']

            if chat_id not in delay:
                delay[chat_id] = []

            delay_d = delay[chat_id]

            print('from {}:"{}".'.format(chat_id, text))
            if len(delay_d) < MAX_PER_MINUTE or delay_d[-MAX_PER_MINUTE] < minute:
                good = False
                for start in STAR_TOKEN:
                    if text[:len(start)] == start:
                        good = True
                        description = text[len(start):]
                if good:
                    delay[chat_id].append(minute)
                    try:
                        result = text_handler.get_link(description)
                    except:
                        greet_bot.send_message(chat_id, 'Я не понял.')
                    else:
                        link = result['link']
                        img = result['img']
                        img_name = 'temp/{}.jpg'.format(result['id'])

                        imageio.imwrite(img_name, img)
                        greet_bot.send_photo(chat_id, img_name)

                        text_handler.plot(description)

                        print('{} for "{}"'.format(link, description))
                        greet_bot.send_message(chat_id,
                                               'Ваш результат для фразы "{}": {}'.format(description, link))

                else:
                    greet_bot.send_message(chat_id, 'Введите "/search <text>" или "/s <text>" для поиска.')
            print('\n\n\n')
            new_offset = last_update_id + 1


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit()
