import numpy as np
import pickle
import time
from gensim.parsing import PorterStemmer
import gensim


class StemmingHelper:
    # This reverse lookup will remember the original forms of the stemmed
    # words

    def __init__(self):
        self.word_lookup = {}
        self.stemmer = PorterStemmer()

    def stem(self, word):
        # Stem the word
        stemmed = global_stemmer.stem(word)

        if stemmed not in self.word_lookup:
            self.word_lookup[stemmed] = {}
        self.word_lookup[stemmed][word] = (
            self.word_lookup[stemmed].get(word, 0) + 1)

        return stemmed

    def original_form(self, word):
        if word in self.word_lookup:
            return max(self.word_lookup[word].keys(),
                       key=lambda x: self.word_lookup[word][x])
        else:
            return word


def load_w2v(limit=2 * 10 ** 5):
    w2v = gensim.models.KeyedVectors.load_word2vec_format(
        'net/GoogleNews-vectors-negative300.bin', binary=True, limit=limit)
    return w2v
