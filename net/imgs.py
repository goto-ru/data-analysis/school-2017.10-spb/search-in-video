import numpy as np
import pickle
import os
import imageio


def imgs_process(dir_name, prob_and_vec):
    MAX_BUCKET = 30

    bucket, names = [], []
    buc_n = 0
    for file_name in os.listdir(dir_name):
        img = imageio.imread(dir_name + '/' + file_name)
        img_np = np.array(img)
        img_np = resize_img(img_np)
        img_np = preprocess(img_np)

        bucket.append(img_np[0])
        names.append(file_name)

        if len(bucket) > MAX_BUCKET:
            result = prob_and_vec(bucket)
            data = {name.split('.')[0]: (result[1][i], result[2][i]) for i, name in enumerate(names)}
            with open('{}/../data/{}.data'.format(dir_name, buc_n), 'wb') as fl:
                pickle.dump(data, fl)
            bucket, names = [], []
            buc_n += 1
            print('Processed {}'.format(buc_n * MAX_BUCKET))


def imgs_load(dir_name):
    imgs = {}
    for file_name in os.listdir(dir_name + '/data'):
        if '.' in file_name and file_name.split('.')[1] == 'data':
            with open('{}/data/{}'.format(dir_name, file_name), 'rb') as fl:
                data = pickle.load(fl)
                imgs.update(data)
    return imgs
