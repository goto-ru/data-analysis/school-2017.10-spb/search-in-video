import numpy as np
import matplotlib.pyplot as plt
import lasagne
import pickle
import time
from net.helper import get_cur_time
from net.data_net import save_net, load_net


def iterate_minibatches(X, y, batchsize):
    X = np.array(X)
    y = np.array(y)
    perm = np.arange(len(X))
    np.random.shuffle(perm)
    for i in range(0, len(X), batchsize):
        r = i + batchsize
        yield (X[perm[i:r]], y[perm[i:r]])


class NeuralNet(dict):
    def __init__(self, must_have=None, **kwargs):
        super().__init__(**kwargs)
        self.must_have = must_have

    def check(self):
        for mh in self.must_have:
            if not hasattr(self, mh):
                raise Exception('NeuralNet must have attrs "{}"!'.format(mh))
        return self


def base_plot(losses_val, losses_train):
    plt.figure(figsize=(16, 8))

    plt.subplot(221)
    plt.plot(np.arange(len(losses_val)) * Training.mod, losses_val, 'r', losses_train, 'b')
    plt.grid()
    plt.legend(['y = loss valid', 'y = loss train'], loc='upper right')

    plt.subplot(222)
    plt.plot(losses_train[-120:])
    plt.grid()
    plt.legend(['y = loss train on last 120'], loc='upper right')

    plt.subplot(223)
    plt.plot(losses_val)
    plt.grid()
    plt.legend(['y = loss valid'], loc='upper right')

    plt.subplot(224)
    plt.plot(losses_val[-60:])
    plt.grid()
    plt.legend(['y = loss valid on last 60'], loc='upper right')

    plt.show()


class Training:
    mod = 10
    mod_epoch = 5
    num_epochs = 2000

    def __init__(self, net, batch_size, vers=''):
        self.epoch = 0
        self.vers = vers
        self.losses_val = []
        self.losses_train = []
        self.net = net
        self.batch_size = batch_size
        self.ploter = base_plot

        # if there are some funs in net we can use thire
        interesting_attrs = ['train_fun', 'loss_fun', 'loss_fun_det']
        for attr in interesting_attrs:
            if hasattr(net, attr):
                setattr(self, attr, getattr(net, attr))

    def clean(self):
        self.epoch = 0
        self.losses_val = []
        self.losses_train = []

    def set_Xy(self, X_train, y_train, X_val, y_val):
        self.X_train = X_train
        self.y_train = y_train
        self.X_val = X_val
        self.y_val = y_val

    def set_fun(self, train_fun, loss_fun, loss_fun_det=None):
        self.train_fun = train_fun
        self.loss_fun = loss_fun
        self.loss_fun_det = loss_fun_det or loss_fun

    def set_ploter(self, ploter):
        self.ploter = ploter

    def save_info(self, file):
        with open(file, 'wb') as fl:
            info = {
                'epoch': self.epoch,
                'losses_train': self.losses_train,
                'losses_val': self.losses_val}
            pickle.dump(info, fl)

    def plot(self):
        self.ploter(self.losses_val, self.losses_train)

    def training(I, dir, format_file='epoch_{}_{}.net', is_plt=True):
        if is_plt:
            from IPython.display import clear_output

        start_time_train = time.time()
        while I.epoch < Training.num_epochs:
            if is_save and I.epoch % Training.mod_epoch == 0:
                save_net(I.net['last'], format_file.format(I.vers, get_cur_time()), dir)

            train_err = 0
            train_batches = 0
            start_time = time.time()

            for i, batch in enumerate(iterate_minibatches(I.X_train, I.y_train, I.batch_size)):
                inputs, targets = batch

                train_err_batch = I.train_fun(inputs, targets)
                I.losses_train.append(train_err_batch)

                train_err += train_err_batch
                train_batches += 1

                if train_batches % Training.mod == 1 or not val_err:
                    val_err = I.loss_fun_det(I.X_val, I.y_val)
                    I.losses_val.append(val_err)

                if is_plt:
                    clear_output(True)
                    I.plot()

                print('LV={:2.3f} LT={:3.3f} AvrTime={:2.3f} Num={} TotTime={:5.1f}m'
                      .format(float(val_err),
                              float(train_err_batch),
                              float(time.time() - start_time) / (i + 1),
                              i,
                              float(time.time() - start_time_train) / 60))

            if not is_plt:
                val_batches = 0
                val_loss = 0

                for inputs, targets in iterate_minibatches(I.X_val, I.y_val, I.batch_size):
                    val_loss += I.loss_fun(inputs, targets)
                    val_batches += 1

                print("Epoch {} of {} took {:.3f}s".
                      format(I.epoch + 1, Training.num_epochs, time.time() - start_time))

                print("  training loss (in-iteration):\t\t{:.6f}".
                      format(train_err / train_batches))

                print("  valid loss (in-iteration):\t\t{:.6f}".
                      format(val_loss / val_batches))

            I.epoch += 1
