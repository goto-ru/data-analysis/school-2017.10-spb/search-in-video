import time
import numpy as np


def procces_x(vects, W):
    return np.pad(vects, [(0, W), (0, 0)], mode='constant', constant_values=0)[:W].T


def sent2words(sent):
    return filter(lambda x: x not in ['', ' ', '<'], re.sub(r'[^a-z ]+', '', str.lower(sent)).split(' '))


def get_cur_time():
    return time.strftime('%d-%m-%Y_%H-%M-%S', time.localtime())
