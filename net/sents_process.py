import numpy as np
import pylab
import pickle
import re
import os
from net.helper import get_cur_time


def sent2words(sent):
    return filter(lambda x: x not in ['', ' ', '<'], re.sub(r'[^a-z ]+', '', str.lower(sent)).split(' '))


def sents_save(sents, dir_name, w2v, save_bad=True):
    MAX_BUCKET = 2000

    bad_words = []
    bad_count = 0
    good_count = 0

    bucket, names = [], []
    buc_n = 0
    for id, text in sents:
        bucket.append(text)
        names.append(id)

        if len(bucket) > MAX_BUCKET:
            matr = []
            for sent in bucket:
                words = sent2words(sent)
                sent_vec = []
                for word in words:
                    try:
                        sent_vec.append(w2v[word])
                        good_count += 1
                    except:
                        if save_bad:
                            sent_vec.append(word)
                        bad_words.append(word)
                        bad_count += 1
                matr.append(sent_vec)

            data = list(zip(names, matr))

            with open('{}/w2v/{}_{}.wtov'.format(dir_name, get_cur_time(), buc_n), 'wb') as fl:
                pickle.dump(data, fl)
            bucket, names = [], []
            buc_n += 1
            print('Processed {}'.format(buc_n * MAX_BUCKET))

    bad_words = list(set(bad_words))
    print('Unique words =', len(bad_words))
    print('Unknown words =', bad_count)
    print('Known words =', good_count)
    return bad_words


def sents_load(dir_name):
    texts = {}
    for file_name in os.listdir(dir_name + '/w2v'):
        if '.' in file_name and file_name.split('.')[1] == 'wtov':
            with open('{}/w2v/{}'.format(dir_name, file_name), 'rb') as fl:
                data = pickle.load(fl)
                for id, v in data:
                    texts[id] = texts.get(id, []) + [v]
    return texts
