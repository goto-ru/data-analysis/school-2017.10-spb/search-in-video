import os
import imageio
import time
from net.vgg16 import build_model, set_weights
from net.process import preprocess, resize_img
import lasagne
import theano.tensor as T
import theano
import numpy as np
import sys
import os
import pickle


def make_network(name='net/vgg16.pkl'):
    net = build_model()
    net = set_weights(net, name=name)

    input_image = T.tensor4('input')
    output_last = lasagne.layers.get_output(net['prob'], input_image)
    output_fc8 = lasagne.layers.get_output(net['fc8'], input_image)
    output_fc7 = lasagne.layers.get_output(net['fc7'], input_image)
    output_fc6 = lasagne.layers.get_output(net['fc6'], input_image)

    prob_and_vec = theano.function(
        [input_image],
        [output_last, output_fc8, output_fc7, output_fc6])
    return net, prob_and_vec


def imgs_process(imgs, prob_and_vec):
    img_np = []
    for img in imgs:
        img = np.array(img)
        img = resize_img(img)
        img = preprocess(img)
        img_np.append(img[0])

    results = prob_and_vec(img_np)

    def net_result(tpl):
        return {
            'prob': tpl[0],
            'fc8': tpl[1],
            'fc7': tpl[2],
            'fc6': tpl[3]
        }

    return [net_result(result) for result in zip(*results)]


def files_process(names, dir_name, prob_and_vec):
    MAX_BUCKET = 30
    bucket, inds = [], []

    def bucket_process():
        return {id: v for id, v in zip(inds, imgs_process(bucket, prob_and_vec))}

    for name in names:
        try:
            image = imageio.imread(dir_name + '/' + name)
            bucket.append(image)
            inds.append(name)
            if len(bucket) >= MAX_BUCKET:
                result = bucket_process()
                bucket, inds = [], []
                yield result
        except:
            pass
    if bucket:
        yield bucket_process()


if __name__ == '__main__':
    argv = {k.split('=')[0]: k.split('=')[1] for k in sys.argv[1:]}

    if 'dir' in argv and 'start' in argv and 'end' in argv and 'to' in argv:
        to_dir = argv['to']
        data_dir = argv['dir']
        start = int(argv['start'])
        end = int(argv['end'])

        print(data_dir, start)
        net, prob_and_vec = make_network()
        print('Network is created')

        names = list(sorted(os.listdir(data_dir)))[start:end]
        print(names[0], names[-1])

        count = 0
        for ind, data in enumerate(files_process(names, data_dir, prob_and_vec)):
            with open('{}/{}.imgs'.format(to_dir, str(start) + '_' + str(ind)), 'wb') as fl:
                pickle.dump(data, fl)
            count += len(data)
            print('Processed {}'.format(count))
    else:
        print('You must to write more flags.')
